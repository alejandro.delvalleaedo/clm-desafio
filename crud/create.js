//const connection = require('../connection.js');

const Movie = require('../models/Movies');

//connection.connect();

async function createMovie(title, year, released, genre, director, actors, plot, rating) {
    const movie = new Movie({
        Title: title,
        Year: year,
        Released: released,
        Genre: genre,
        Director: director,
        Actors: actors,
        Plot: plot,
        Rating: rating
    })

    const movieSaved = await movie.save();
    return movieSaved;
}
exports.createMovie = createMovie;

//createMovie();