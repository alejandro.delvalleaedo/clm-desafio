//const connection = require('../connection.js');

const Movie = require('../models/Movies');

//connection.connect();

async function getMovie(mName) {
    const movie = await Movie.findOne({Title: mName}); //Busca por titulo
    //console.log(movie);
    return movie;
}

exports.getMovie = getMovie;