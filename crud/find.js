/*
Lista Todo
*/

//const connection = require('../connection.js');

const Movie = require('../models/Movies');

//connection.connect();

async function findAll() {
    const movie = await Movie.find();
    //console.log(movie);
    return movie;
}

//findAll();
exports.findAll = findAll;