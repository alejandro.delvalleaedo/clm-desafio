const connection = require('../connection.js');

const Movie = require('../models/Movies');

connection.connect();

async function deleteMovie() {
    const result = await Movie.findOneAndDelete({Title: 'Gravity'});
    console.log(result);
}

deleteMovie();