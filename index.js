const { default: axios } = require('axios');
const koa = require('koa');
const bodyParser = require('koa-bodyparser');
const Router = require('koa-router');
const connection = require('./connection.js');
const Movie = require('./models/Movies.js');
const create = require('./crud/create.js');
const findAll = require('./crud/find.js');
const findOne = require('./crud/findOne.js');

const Joi = require('joi');

const app = new koa();
const router = new Router();
app.use(bodyParser());


//Settings
const apiKey = '797bbf13';
const urlParam = 'tt3896198';

//Conection
connection.connect();

//joi
const schema = Joi.object().keys({ 
  movieName: Joi.string().required(),
  year: Joi.number(), 
}); 


//Get Method
router.get('/add/:movieName', async (ctx, next) => {
  try {
    var movieName = ctx.params.movieName; //Lee de la URL el nombre de la pelicula
    const year = ctx.request.header.year;
    const obj = {
      movieName, year
    }
    const result = schema.validate(obj); 
    if(result.error) throw new Error(JSON.stringify(result.error));
    if(!ctx.request.header.year){
      console.log(ctx.request.header.year);
      const response = await axios.get(`http://www.omdbapi.com/?i=${urlParam}&apikey=${apiKey}&t=${movieName}`) //Dirección de la API
      var rdo = [response.data['Title'], response.data['Year'], response.data['Released'], response.data['Genre'], response.data['Director'], response.data['Actors'], response.data['Plot'], response.data['Ratings']];
    ctx.body = await create.createMovie(rdo[0], rdo[1] , rdo[2], rdo[3], rdo[4], rdo[5], rdo[6], rdo[7]); //Inserta los datos en la base de datos
    }else{
      const response = await axios.get(`http://www.omdbapi.com/?i=${urlParam}&apikey=${apiKey}&t=${movieName}&y=${year}`) //Dirección de la API
      var rdo = [response.data['Title'], response.data['Year'], response.data['Released'], response.data['Genre'], response.data['Director'], response.data['Actors'], response.data['Plot'], response.data['Ratings']];
    ctx.body = await create.createMovie(rdo[0], rdo[1] , rdo[2], rdo[3], rdo[4], rdo[5], rdo[6], rdo[7]); //Inserta los datos en la base de datos
    }
  } catch (error) {
    ctx.body = error.message;
  }
  })

router.get('/showlist', async (ctx, next) => {

  
  if(ctx.request.header.pagina || ctx.request.header.pagina >= 0){
    ctx.body = await Movie.find().limit(5).skip((ctx.request.header.pagina-1)*5);
  }else{
    ctx.body = 'error';
  }


}) 

//Post Method

router.post('/', async (ctx, next) => {
    var search = ctx.request.body;
    //console.log(search["movie"]);
    var mPlot = await findOne.getMovie(search["movie"]);
    //ctx.body = mPlot["Plot"];
    let word = search["find"];
    //console.log(search);
    //console.log(mPlot["Plot"]);
    const newPlot = mPlot["Plot"].replace(word, search["replace"]);
    ctx.body = newPlot;
    

})

app.use(router.routes());
app.use(router.allowedMethods());

app.listen(3000, () => {
    console.log('Server on port 3000');
})