const connection = require('./connection.js');

const Movie = require('./models/Movies');

connection.connect();

async function main() {
    const movie = new Movie({
        Title: 'TestMovie 2',
        Year: 2006,
        Released: new Date(),
        Genre: 'Action',
        Director: 'James Cameron',
        Actors: 'Brad Pitt, Leonardo DaVinci, Jennifer Lopez',
        Plot: 'What is a Plot',
        Rating: 'Mature'
    })

    const movieSaved = await movie.save();
    console.log(movieSaved);
}

main();

