const { Schema, model } = require('mongoose');

const movieSchema = new Schema({
    Title: {
        type: String,
        unique: true,
        required: true
    },
    Year: Number,
    Released: { type: Date, default: Date.now },
    Genre: String,
    Director: String,
    Actors: String,
    Plot: String,
    Rating: { type: Array}
});

module.exports = model('Movie', movieSchema);