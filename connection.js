/*const mongoose = require('mongoose');
const { mainModule } = require('process');

main().catch(err => console.log(err));

async function main(){
    await mongoose.connect('mongodb://localhost:27017/test');

    const moviesSchema = new mongoose.Schema({
        Title: String,
        Year: Number,
        Released: { type: Date, default: Date.now },
        Genre: String,
        Director: String,
        Actors: String,
        Plot: String,
        Rating: String
    });

    const movies = mongoose.model('movies', moviesSchema);


    mongoose.connection.on('open', _ => {
        console.log('Database is connected to test');
    })
}

function test(){
    console.log('test');
}

exports.test = test;
exports.main = main;
*/
const mongoose = require('mongoose');
const uri = 'mongodb://127.0.0.1:27017/moviesdb';



function connect(){

    mongoose.connect(uri, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }).catch(err => console.log(err));

    mongoose.connection.once('open', _=> {
        console.log('Database is connected to ', uri);
    });

    mongoose.connection.on('error', err => {
        console.log(err);
    });
}

exports.connect = connect;
